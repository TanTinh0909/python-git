# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals
import math
import numpy as np 

import matplotlib.pyplot as plt

import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation 

x0 = 5

def cost(x):
    return (x**2+3*x+3)/(x+1)

def grad(x):
    return (x*(x+2))/(x+1)**2

def Momentum(x0, eta = 0.1, gamma = 0.9):
    v = [0]
    x = [x0]
    for it in range(100):
        g = grad(x[-1])
        if abs(g) < 1e-6:
            break
        v_new = gamma*v[-1] + eta*g
        x_new = x[-1] - v_new
        v.append(v_new)
        x.append(x_new)
    print("f(x)=(x^2+3x+3)/(x+1) dat cuc tieu = ",x_new)
    return (np.asarray(x), v, it)


def viz_alg_1d(x, cost, filename = 'momentum2d.gif'):
#     x = x.asarray()
    it = len(x)
    y = cost(x)
    xmin, xmax = np.min(x), np.max(x)
    ymin, ymax = np.min(y), np.max(y)
    
    xmin, xmax = -4, 6
    ymin, ymax = -12, 25
    
    x0 = np.linspace(xmin-1, xmax+1, 1000)
    y0 = cost(x0)
       
    fig, ax = plt.subplots(figsize=(4, 4))  
    
    def update(i):
        ani = plt.cla()
        plt.axis([xmin, xmax, ymin, ymax])
        plt.plot(x0, y0)
        ani = plt.title('$f(x) = (x^2+3x+3)/(x+1); x_0 = 5; \eta = 0.1; \gamma = 0.9$')
        if i == 0:
            ani = plt.plot(x[i], y[i], 'ro', markersize = 7)
        else:
            ani = plt.plot(x[i-1], y[i-1], 'ok', markersize = 7)
            ani = plt.plot(x[i-1:i+1], y[i-1:i+1], 'k-')
            ani = plt.plot(x[i], y[i], 'ro', markersize = 7)
        label = 'GD with Momemtum 2: iter %d/%d' %(i, it)
        ax.set_xlabel(label)
        return ani, ax 
        
    anim = FuncAnimation(fig, update, frames=np.arange(0, it), interval=200)
    anim.save(filename, dpi = 100, writer = 'imagemagick')
    plt.show()
    
# x = np.asarray(x)
(x, v, it) = Momentum(-5, 0.1, .9)
viz_alg_1d(x, cost)