# To support both python 2 and python 3
from __future__ import division, print_function, unicode_literals
import math
import numpy as np 

import matplotlib.pyplot as plt

import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation 

x0 = 5

def cost(x):
    return (2*x-1)/(x+2)

def grad(x):
    return 5/(x+2)**2

def GD_newton(x0):
    x = [x0]
    for it in range(100):
        if abs(cost(x[-1])) < 1e-6 or abs(grad(x[-1])) < 1e-6:
            break
        x_new = x[-1] - 3*cost(x[-1])/grad(x[-1])
        print(x_new, cost(x[-1]), grad(x[-1]))
        x.append(x_new)
    print("f(x)=(2x-1)/(x+2) dat cuc tieu = ",x_new)
    return (x, it)

GD_newton(-10)

def plot_fn(fn, xmin = -5, xmax = 5, xaxis = True, opts = 'b-'):
    x = np.linspace(xmin, xmax, 1000)
    y = fn(x)
    ymin = np.min(y) - .5
    ymax = np.max(y) + .5
    plt.axis([xmin, xmax, ymin, ymax])
    if xaxis:
        x0 = np.linspace(xmin, xmax, 2)
        plt.plot([xmin, xmax], [0, 0], 'k')
    plt.plot(x, y, opts)
plot_fn(cost, -10, 10)

plt.show()